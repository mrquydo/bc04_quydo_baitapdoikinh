export const renderGlassesList = (dataGlasses) => {
  let glassesListContent = "";

  dataGlasses.forEach((glass) => {
    glassesListContent += `
    <a class="col-4" >
        <img onclick =showThongTin('${glass.id}') src="${glass.src}" alt="" class="img-fluid" >
    </a>`;
  });

  document.getElementById("vglassesList").innerHTML = glassesListContent;
};

export const renderGlassesInfo = (glass) => {
  return `
    <h3>${glass.name} - ${glass.brand} (${glass.color})</h3>
    <span>${glass.price}$</span>
    <p>${glass.description}</p> `;
};

export const renderVirtualGlass = (glass) => {
  return `
  <div id="vtglass">
  <img src="${glass.virtualImg}" alt="" class="virtual-glass">
  </div>`;
};

// Tìm index của kính

export function timKiemViTri(id, glassList) {
  for (var index = 0; index < glassList.length; index++) {
    var glass = glassList[index];
    if (glass.id == id) {
      return index;
    }
  }
  //  ko tìm thấy
  return -1;
}
